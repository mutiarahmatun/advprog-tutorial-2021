package id.ac.ui.cs.advprog.tutorial2.command.repository;

import id.ac.ui.cs.advprog.tutorial2.command.core.spell.Spell;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class ContractSeal {

    private Map<String, Spell> spells;
    Spell latestSpell;
    int time;

    public ContractSeal() {
        spells = new HashMap<>();
    }

    public void registerSpell(Spell spell) {
        spells.put(spell.spellName(), spell);
    }

    public void castSpell(String spellName) {
        // TODO: Complete Me
        spells.get(spellName).cast();
        latestSpell = spells.get(spellName);
        time = 0;

    }

    public void undoSpell() {
        // TODO: Complete Me
        if(time == 0) {
            latestSpell.undo();
            time = 1;
        }
    }

    public Collection<Spell> getSpells() { return spells.values(); }
}
