package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.List;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    private List<Spell> chain;

    public ChainSpell(List<Spell> something) {
        chain = something;
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }

    @Override
    public void cast() {
        for(Spell a : chain) {
            a.cast();
        }
    }

    @Override
    public void undo() {
        for(Spell b : chain) {
            b.undo();
        }
    }

}
