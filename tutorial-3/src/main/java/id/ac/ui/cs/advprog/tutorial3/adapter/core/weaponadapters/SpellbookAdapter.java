package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :)
public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private boolean isChargedAttackCasted;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
        this.isChargedAttackCasted = false;
    }

    @Override
    public String normalAttack() {
        this.isChargedAttackCasted = false;
        return spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if (!this.isChargedAttackCasted) {
            this.isChargedAttackCasted = !this.isChargedAttackCasted;
            return spellbook.largeSpell();
        } else {
            this.isChargedAttackCasted = !this.isChargedAttackCasted;
            return "Magic power not enough for large spell";
        }
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }

}
