package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AgileAdventurer extends Adventurer {
    //ToDo: Complete me
    public AgileAdventurer(){
        AttackBehavior attackBehavior = new AttackWithGun();
        this.setAttackBehavior(attackBehavior);
        DefenseBehavior defenseBehavior = new DefendWithBarrier();
        this.setDefenseBehavior(defenseBehavior);
    }

    @Override
    public String getAlias(){ return "Top global Agile"; }

}
