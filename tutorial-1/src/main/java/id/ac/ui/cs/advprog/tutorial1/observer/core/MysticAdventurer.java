package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {

    public MysticAdventurer(Guild guild) {
        this.name = "Mystic";
        //ToDo: Complete Me
        this.guild = guild;
        this.guild.add(this);

    }

    //ToDo: Complete Me

    @Override
    public void update(){
        if ((this.guild.getQuestType().equalsIgnoreCase("Delivery")
                || this.guild.getQuestType().equalsIgnoreCase("Escort"))){
            getQuests().add(this.guild.getQuest());
        }
    }

}
