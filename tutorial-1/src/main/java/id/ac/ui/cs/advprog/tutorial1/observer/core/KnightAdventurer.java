package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class KnightAdventurer extends Adventurer {

    public KnightAdventurer(Guild guild) {
        this.name = "Knight";
        //ToDo: Complete Me
        this.guild = guild;
        this.guild.add(this);

    }

    //ToDo: Complete Me

    @Override
    public void update(){
        if ((this.guild.getQuestType().equalsIgnoreCase("Delivery")
                || this.guild.getQuestType().equalsIgnoreCase("Rumble")
                || this.guild.getQuestType().equalsIgnoreCase("Escort"))){
            getQuests().add(this.guild.getQuest());
        }
    }

}
