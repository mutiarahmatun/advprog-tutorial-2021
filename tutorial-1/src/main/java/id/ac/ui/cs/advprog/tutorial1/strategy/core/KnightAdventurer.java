package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class KnightAdventurer extends Adventurer {
    //ToDo: Complete me
    public KnightAdventurer(){
        AttackBehavior attackBehavior = new AttackWithSword();
        this.setAttackBehavior(attackBehavior);
        DefenseBehavior defenseBehavior = new DefendWithArmor();
        this.setDefenseBehavior(defenseBehavior);
    }


    public String getAlias(){
        return "Top global Knight";
    }

}
