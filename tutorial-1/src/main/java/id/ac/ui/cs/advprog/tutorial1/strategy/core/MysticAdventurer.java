package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class MysticAdventurer extends Adventurer {
    //ToDo: Complete me
    public MysticAdventurer(){
        AttackBehavior attackBehavior = new AttackWithMagic();
        setAttackBehavior(attackBehavior);
        DefenseBehavior defenseBehavior = new DefendWithShield();
        setDefenseBehavior(defenseBehavior);
    }


    public String getAlias(){
        return "Top global Mystic";
    }

}
