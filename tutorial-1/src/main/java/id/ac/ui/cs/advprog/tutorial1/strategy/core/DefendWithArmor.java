package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
    //ToDo: Complete me
    public DefendWithArmor(){}

    public String defend(){
        return "Armor Defend!!!";
    }

    @Override
    public String getType() {
        return "Armor Type";
    }

}
